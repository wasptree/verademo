pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                // Compile Java app
                sh 'mvn clean package'
                // pull docker container
                sh 'docker pull wasptree/verademo-web'
            }
        }
        stage('Security Scan Master Branch') {
            when {
                branch "master"
            }
            steps {
                parallel(
                    a:{
                        // Policy scan
                        catchError(buildResult: 'SUCCESS', stageResult: 'UNSTABLE', message: 'Veracode Policy Scan Failure') {
                            withCredentials([usernamePassword(credentialsId: 'VeracodeAPI', passwordVariable: 'VERACODEKEY', usernameVariable: 'VERACODEID')]) {
                            veracode applicationName: "Verademo_Jenkins", criticality: 'VeryHigh',
                            fileNamePattern: '', replacementPattern: '', scanExcludesPattern: '', scanIncludesPattern: '',
                            scanName: 'build $buildnumber - Jenkins',
                            canFailJob: true,
                            uploadExcludesPattern: '', uploadIncludesPattern: 'target/*.war', waitForScan: true, timeout: 30,
                            vid: VERACODEID, vkey: VERACODEKEY
                        }
                        }
                    },
                    b:{
                        // 3rd party scan application
                        withCredentials([string(credentialsId: 'SRCCLR_API_TOKEN', variable: 'SRCCLR_API_TOKEN')]) {
                            sh 'curl -sSL https://download.sourceclear.com/ci.sh | sh'
                        }
                    },
                    c:{
                        // 3rd party scan docker container
                        withCredentials([string(credentialsId: 'SRCCLR_API_TOKEN', variable: 'SRCCLR_API_TOKEN')]) {
                            sh 'curl -sSL https://download.sourceclear.com/ci.sh | sh -s scan --image wasptree/verademo-web'
                        }
                    }
                )
            }
        }
        stage('Security Scan Feature Branch'){
            when {
                branch "release"
            }
            steps {
                parallel(
                    a:{
                        // Sandbox scan
                        withCredentials([usernamePassword(credentialsId: 'VeracodeAPI', passwordVariable: 'VERACODEKEY', usernameVariable: 'VERACODEID')]) {
                            veracode applicationName: "Verademo_Jenkins", criticality: 'VeryHigh', createSandbox: true, sandboxName: "jenkins-release", 
                            fileNamePattern: '', replacementPattern: '', scanExcludesPattern: '', scanIncludesPattern: '',
                            scanName: 'build $buildnumber - Jenkins',
                            uploadExcludesPattern: '', uploadIncludesPattern: 'target/*.war', waitForScan: true, timeout: 30,
                            vid: VERACODEID, vkey: VERACODEKEY
                        }
                    },
                    b:{
                        // 3rd party scan application
                        withCredentials([string(credentialsId: 'SRCCLR_API_TOKEN', variable: 'SRCCLR_API_TOKEN')]) {
                            sh 'curl -sSL https://download.sourceclear.com/ci.sh | sh'
                        }
                    },
                    c:{
                        // 3rd party scan docker container
                        withCredentials([string(credentialsId: 'SRCCLR_API_TOKEN', variable: 'SRCCLR_API_TOKEN')]) {
                            sh 'curl -sSL https://download.sourceclear.com/ci.sh | sh -s scan --image wasptree/verademo-web'
                        }
                    }
                )
            }
        }
        stage('Security Scan Development Branch'){
            when {
                branch "development"
            }
            steps{
                parallel(
                    a:{
                        //Pipeline scan
                        catchError(buildResult: 'SUCCESS', stageResult: 'UNSTABLE', message: 'Veracode Policy Scan Failure') {
                            withCredentials([usernamePassword(credentialsId: 'VeracodeAPI', passwordVariable: 'VERACODEKEY', usernameVariable: 'VERACODEID')]) {
                                sh 'curl -O https://downloads.veracode.com/securityscan/pipeline-scan-LATEST.zip'
                                sh 'unzip -o pipeline-scan-LATEST.zip pipeline-scan.jar'
                                sh '''java -jar pipeline-scan.jar -vid "$VERACODEID" -vkey "$VERACODEKEY" --file target/verademo.war'''
                            }
                        }
                    },
                    b:{
                        // 3rd party scan application
                        withCredentials([string(credentialsId: 'SRCCLR_API_TOKEN', variable: 'SRCCLR_API_TOKEN')]) {
                            sh 'curl -sSL https://download.sourceclear.com/ci.sh | sh'
                        }
                    },
                    c:{
                        // 3rd party scan docker container
                        withCredentials([string(credentialsId: 'SRCCLR_API_TOKEN', variable: 'SRCCLR_API_TOKEN')]) {
                            sh 'curl -sSL https://download.sourceclear.com/ci.sh | sh -s scan --image wasptree/verademo-web'
                        }
                    }
                )
            }
        }
        stage ('Deploy Application into docker and start docker'){
            when {
                anyOf{
                    branch 'master';
                    branch 'release'
                }
            }
            steps{
                // Deploy Application into docker and start docker
                echo 'Dummy step - Deploy Application'
                echo '[+] deploying application to staging environment'
            }
        }
        stage ('Security Scan - Dynamic Analysis'){
            when {
                anyOf{
                    branch 'master';
                    branch 'release'
                }
            }
            steps {
                // Dynamic Analysis
                withCredentials([usernamePassword(credentialsId: 'VeracodeAPI', passwordVariable: 'VERACODEKEY', usernameVariable: 'VERACODEID')]) {
                    veracodeDynamicAnalysisResubmit analysisName: 'verademo-web', maximumDuration: 72, vid: VERACODEID, vkey: VERACODEKEY
                }
            }
        }
    }
}
